import WebNotes.models as models
import WebNotes.database_config as conf
from datetime import datetime

#session = models.get_session()
session = conf.get_session()

##
# Saves the contents of a given file
# a version of 0 means the file has never been saved before
# returns -1 if there was an issue with the queries
##
def save_page_contents(contents, file_id, name, owner, version):    
    last_version = 0

    if version != 0:
        last_version_page = session.query(models.Page).filter_by(owner_id=owner, id=file_id)\
                .order_by(models.Page.version.desc()).first()
        if last_version_page == None:
            return -1
        print("Last version " + str(last_version_page.version))

        last_version = last_version_page.version

    new_version = last_version + 1
    
    print(new_version)

    new_page = models.Page(id=file_id, version=new_version, name=name, contents=contents, \
            last_edit=datetime.now(), owner_id=owner)
    session.add(new_page)
    session.commit()

def create_new_page(name, owner):
    last_page = session.query(models.Page).order_by(models.Page.id.desc()).first()

    last_file_id = 0

    if not last_page == None:
        last_file_id = last_page.id

    new_id = last_file_id + 1

    new_page = models.Page(id=new_id, version=0, name=name, contents="", \
            last_edit=datetime.now(), owner_id=owner)
    session.add(new_page)
    session.commit()

    return new_id