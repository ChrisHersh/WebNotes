from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

db_username = "root"
db_password = "mysql"
db_host = "192.168.100.100"

conn_str = "mysql+pymysql://{}:{}@{}:3306/webnotes".format(db_username, db_password, db_host)

#SQLite database, only for testing
#engine = create_engine('sqlite:///store.db', echo=True)

##
# For test purposes this database is living in a local virtual machine
##
engine = create_engine(conn_str)

Session = sessionmaker(bind=engine)

session = Session()

def get_session():
    return session