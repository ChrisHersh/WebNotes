import WebNotes.models as models
import WebNotes.database_config as conf
from datetime import datetime

#session = models.get_session()
session = conf.get_session()
    
def create_user(name, password, email):
    #TODO: set id to be auto incrementing
    
    #TODO: actually do this
    pw_hash = password
    
    new_user = models.User(username=name, pw_hash=pw_hash, email=email, admin=False)
    session.add(new_user)
    session.flush()
    
    new_id = new_user.id
    new_tree = models.Tree(json="{}", owner=new_id)
    session.add(new_tree)
    
    session.commit()

    return new_id
        