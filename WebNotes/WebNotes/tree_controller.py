import WebNotes.models as models
import WebNotes.database_config as conf
from datetime import datetime
import json
import WebNotes.page_controller as page_controller

#session = models.get_session()
session = conf.get_session()

def get_tree_object(owner):
    try:
        tree = session.query(models.Tree).filter_by(owner=owner).one()
    except:
        #TODO: Catch the correct exception
        #TODO: log this error
        return -1
        
    return tree

#Child type should either be 'p' or 'f' for page and folder
def add_child(owner, parent_id, child_name, child_type):
    tree_obj = get_tree_object(owner)
    json_string = tree_obj.json
    if json_string == -1:
        #TODO: log error?
        #TODO: prevent this from breaking everything
        pass

    json_data = json.loads(json_string)

    node = check_children_for_id(json_data, parent_id)
    if node == -1:
        #TODO: parent not found, something went wrong
        return -1
    else:
        if child_type == 'p':
            new_item = create_node_new_page(child_name, owner)
        elif child_type == 'f':
            new_item = create_node_folder(child_name, owner)
        else:
            return -1
        new_item = None
        print(node)
        node["children"].append(new_item)
        tree_obj.json = json.dumps(json_data)
        session.commit()
        return 0
        
    

#FOR THE LOVE OF GOD DO NOT MAKE CIRCULAR DEPENDENCIES
#Although if you do, you probably deserve a medal of some sort
#-1 means it was not found
#0 means it was found, used to get the parent
#   0 should not be seen outside of this method
def check_children_for_id(children, id):
    #for c in children:
    if children["id"] == str(id):
        #Should return a pointer to the array
        return children
    #elif len(children["children"]) != 0:
    for c in children["children"]:
        print(c)
        temp = check_children_for_id(c, id)
        if temp != -1:
            return temp
    return -1
    
#Should only be called from a method that is commiting session changes afterwards
def create_node_new_page(name, owner):
    new_id = page_controller.create_new_page(name, owner)
    new_node = {"id":str(new_id), "children":[], "text":name}
    return new_node

#Should only be called from a method that is commiting session changes afterwards
def create_node_folder(name, owner):
    #Change this to something less ... one-ny
    new_id = models.Folder(name=name, owner_id=owner)
    session.add(new_id)
    session.flush()
    new_node = {"id":str(new_id.id), "children":[], "text":name}
    return new_node
