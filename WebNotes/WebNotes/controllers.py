import WebNotes.models as models
import WebNotes.database_config as conf
from datetime import datetime
import json

#session = models.get_session()
session = conf.get_session()

###
# Pages
###

##
# Saves the contents of a given file
# a version of 0 means the file has never been saved before
# returns -1 if there was an issue with the queries
##
def save_page_contents(contents, file_id, name, owner, version):    
    last_version = 0

    if version != 0:
        last_version_page = session.query(models.Page).filter_by(owner_id=owner, id=file_id)\
                .order_by(models.Page.version.desc()).first()
        if last_version_page == None:
            return -1
        print("Last version " + str(last_version_page.version))

        last_version = last_version_page.version

    new_version = last_version + 1
    
    print(new_version)

    new_page = models.Page(id=file_id, version=new_version, name=name, contents=contents, \
            last_edit=datetime.now(), owner_id=owner)
    session.add(new_page)
    session.commit()

def create_new_page(name, owner):
    last_page = session.query(models.Page).order_by(models.Page.id.desc()).first()

    last_file_id = 0

    if not last_page == None:
        last_file_id = last_page.id

    new_id = last_file_id + 1

    new_page = models.Page(id=new_id, version=0, name=name, contents="", \
            last_edit=datetime.now(), owner_id=owner)
    session.add(new_page)
    session.commit()

    return new_id
    
###
# Tree
###
    
def get_tree(owner):
    try:
        tree = session.query(models.Tree).filter_by(models.Tree.json).one()
    except:
        #TODO: Catch the correct exception
        #TODO: log this error
        return -1
        
    return tree

def add_child(owner, parent_id, child_name, child_type):
    json_string = get_tree(owner)
    if json_string == -1:
        #TODO: log error?
        #TODO: prevent this from breaking everything
        pass

    json_data = json.loads(json_string)

    node = check_children_for_id(json_data, parent_id)
    if node == -1:
        #TODO: parent not found, something went wrong
        return -1
    else:
        #TODO: make this actually do things
        new_item = None
        node["children"].append(new_item)
    

#FOR THE LOVE OF GOD DO NOT MAKE CIRCULAR DEPENDENCIES
#Although if you do, you probably deserve a medal of some sort
def check_children_for_id(children, id):
    for c in children:
        if c["id"] == id:
            #Should return a pointer to the array
            return c["id"]
        elif len(c["children"]) == 0:
            continue
        else:
            temp = check_children_for_id(c["children"], id)
            if temp != -1:
                return temp
    return -1
    
def create_json_new_page(name, owner):
    new_id = create_new_page(name, owner)
    new_node = {"id":str(new_id), "children":[], "text":name}
    return new_node

def create_json_folder(owner, name):
    new_node = {"id":str(new_id), "children":[], "text":name}
    return new_node

###
# User
###
    
def create_user(name, password, email):
    #TODO: set id to be auto incrementing
    
    #TODO: actually do this
    pw_hash = password
    
    new_user = User(name, pw_hash, email, False)
    session.add(new_user)
    
    new_id = User.id
    new_tree = Tree("", new_id)
    session.add(new_tree)
    
    session.commit()
        