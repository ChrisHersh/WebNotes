import unittest
from WebNotes.page_controller import *
import WebNotes.models as model
from WebNotes.database_config import *
import common_tests as ct

class Test_controllers(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        ct.init_database()

    def test_save_page_contents(self):
        contents = "# Header\nThis is random text"
        file_id = 1
        name = "bob"
        owner = 1
        version = 1

        self.assertNotEqual(save_page_contents(contents, file_id, name, owner, version), -1)

        last_version_page = session.query(model.Page).filter_by(owner_id=owner, id=file_id)\
                .order_by(model.Page.version.desc()).first()

        self.assertEqual(last_version_page.contents, contents)

    def test_create_new_page(self):
        session = get_session()
        owner = 1
        name = "test_page"
    
        new_id = create_new_page(name, owner)

        page = session.query(model.Page).filter_by(name=name, owner_id=owner, id=new_id).one()

        self.assertEqual(page.contents, "")
        self.assertEqual(new_id, 6)

if __name__ == '__main__':
    unittest.main()
