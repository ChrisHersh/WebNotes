import WebNotes.models as model


def init_database():
    model.TEST = True
    model.drop_tables()
    model.create_tables()

    with open("tests/test_SQL/webnotes_user.sql", "r") as f:
        sql = f.read()
        model.get_session().execute(sql)
    with open("tests/test_SQL/webnotes_tree.sql", "r") as f:
        sql = f.read()
        model.get_session().execute(sql)
    with open("tests/test_SQL/webnotes_page.sql", "r") as f:
        sql = f.read()
        model.get_session().execute(sql)