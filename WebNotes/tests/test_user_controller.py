import unittest
from WebNotes.user_controller import *
import WebNotes.models as model
from WebNotes.database_config import *
import common_tests as ct

class Test_test_user_controller(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        ct.init_database()
    
    def test_create_user(self):
        pw_hash = "password"
        name = "Bob"
        email = "Bob@Bob.com"
        
        new_id = create_user(name, pw_hash, email)

        user = get_session().query(model.User).filter_by(username=name).one()

        self.assertEqual(new_id, 5)
        self.assertEqual(user.username, name)
        self.assertEqual(user.pw_hash, pw_hash)
        self.assertEqual(user.email, email)
        self.assertEqual(user.admin, False)

    def test_tree_init(self):
        pw_hash = "password"
        name = "Bobby"
        email = "Bob@Bob.com"
        
        new_id = create_user(name, pw_hash, email)

        tree = get_session().query(model.Tree).filter_by(owner=new_id).one()
   
        self.assertEqual(tree.json, "{}")

if __name__ == '__main__':
    unittest.main()
