import unittest
from WebNotes.tree_controller import *
import WebNotes.models as model
from WebNotes.database_config import *
import common_tests as ct
import json

class Test_test_tree_controller(unittest.TestCase):
    def setUp(self):
        ct.init_database()

    def test_get_tree(self):
        owner = 1
        db_json = {"id":"1", "children":[]}

        tree_json = get_tree_object(owner)

        #prevents ordering issues by loading the json into an object
        self.assertEqual(json.loads(tree_json.json), db_json)

    def test_add_child_page(self):
        owner = 1
        parent_id = 1
        child_name = "child page"
        child_type = 'p'

        result = add_child(owner, parent_id, child_name, child_type)
        self.assertNotEquals(result, -1)

        modified_json = get_session().query(model.Tree).filter_by(owner=owner).one().json

        expected_node = {"id":"1", "children":[create_node_new_page(child_name, owner)]}

        expected_json = json.dumps(expected_node)

        self.assert_(modified_json, expected_json)

    def test_add_child_folder(self):
        owner = 1
        parent_id = 1
        child_name = "child folder"
        child_type = 'f'

        result = add_child(owner, parent_id, child_name, child_type)
        self.assertNotEquals(result, -1)

        modified_json = get_session().query(model.Tree).filter_by(owner=owner).one().json

        expected_node = {"id":"1", "children":[create_node_folder(child_name, owner)]}

        expected_json = json.dumps(expected_node)

        self.assert_(modified_json, expected_json)

    def test_create_node_new_page(self):
        name = "child page"
        owner = 1

        expected = {"id":'6', "children":[], "text":name}

        result_node = create_node_new_page(name, owner)
        
        self.assertEquals(result_node, expected)

    def test_create_node_folder(self):
        name = "child folder"
        owner = 1

        expected = {"id":'1', "children":[], "text":name}

        result_node = create_node_folder(name, owner)
        
        self.assertEquals(result_node, expected)

    def test_check_children_for_id(self):
        #TODO: test this method, don't forget to check the recursion
        
        #Using compact json to save space, has nodes 1 through 6
        test_json = '{"id":"1","children":[{"id":"2","children":[{"id":"3","children":[{"id":"4","children":[]},{"id":"5","children":[]},{"id":"6","children":[]}]}]}]}'
        test_node = json.loads(test_json)

        node_1 = check_children_for_id(test_node, 1)

        self.assertEquals(node_1["id"], "1")
        self.assertEquals(len(node_1["children"]), 1)

        node_3 = check_children_for_id(test_node, 3)

        self.assertEquals(node_3["id"], "3")
        self.assertEquals(len(node_3["children"]), 3)

        node_6 = check_children_for_id(test_node, 6)

        self.assertEquals(node_6["id"], "6")
        self.assertEquals(len(node_6["children"]), 0)

if __name__ == '__main__':
    unittest.main()
